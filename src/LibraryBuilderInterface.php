<?php

namespace Drupal\whiteboard;

/**
 * JsDraw2D library builder interface.
 */
interface LibraryBuilderInterface {

  const JSDRAW2D_URL = 'https://raw.githubusercontent.com/sameerb/jsDraw2D/master/jsDraw2D.js';

  const LIBRARY_PATH = '/libraries/jsdraw2d/';

  /**
   * Builds a definition for jsDraw2D library.
   *
   * @return array
   *   jsDraw2D library definition.
   */
  public function build();

}
