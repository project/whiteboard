<?php

namespace Drupal\whiteboard;

/**
 * Whiteboard library builder.
 */
class LibraryBuilder implements LibraryBuilderInterface {

  /**
   * {@inheritdoc}
   */
  public function build() {

    $library = [
      'remote' => 'http://jsdraw2d.jsfiction.com',
      'version' => 'Beta 1.1.2',
      'license' => [
        'name' => 'LGPL',
        'url' => 'https://github.com/sameerb/jsDraw2D/blob/master/LGPL_License.txt',
        'gpl-compatible' => TRUE,
      ],
    ];

    return $library;
  }

}
