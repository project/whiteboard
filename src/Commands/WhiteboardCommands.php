<?php

namespace Drupal\whiteboard\Commands;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Asset\LibraryDiscoveryInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\State\StateInterface;
use Drupal\whiteboard\LibraryBuilderInterface;
use Drush\Commands\DrushCommands;
use GuzzleHttp\Client;

/**
 * Drush integration for Whiteboard module.
 *
 * @todo Crete a test for this.
 */
class WhiteboardCommands extends DrushCommands {

  /**
   * Library discovery service.
   *
   * @var \Drupal\Core\Asset\LibraryDiscoveryInterface
   */
  protected $libraryDiscovery;

  /**
   * The HTTP client.
   *
   * @var \GuzzleHttp\Client
   */
  protected $httpClient;

  /**
   * The state key value store.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * The time service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $time;

  /**
   * Constructs the object.
   *
   * @param \Drupal\Core\Asset\LibraryDiscoveryInterface $library_discovery
   *   The library discovery service.
   * @param \GuzzleHttp\Client $http_client
   *   The HTTP client.
   * @param \Drupal\Core\State\StateInterface $state
   *   The state key value store.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time service.
   */
  public function __construct(
    LibraryDiscoveryInterface $library_discovery,
    Client $http_client,
    StateInterface $state,
    TimeInterface $time,
  ) {
    parent::__construct();
    $this->libraryDiscovery = $library_discovery;
    $this->httpClient = $http_client;
    $this->state = $state;
    $this->time = $time;
  }

  /**
   * Downloads the Markitup library required for TeXbar.
   *
   * @command whiteboard:download
   * @aliases whiteboard-download
   */
  public function downloadWhiteboard() {
    $io = $this->io();

    $source = LibraryBuilderInterface::JSDRAW2D_URL;

    $destination = DRUPAL_ROOT . LibraryBuilderInterface::LIBRARY_PATH;

    \Drupal::service('file_system')->prepareDirectory($destination, FileSystemInterface::CREATE_DIRECTORY);

    $this->httpClient->get($source, ['sink' => $destination .= '/jsdraw2d.js']);

    if (is_file($destination)) {
      $io->success("jsDraw2D library has been downloaded into the destination directory.");
    }
    else {
      $io->error("jsDraw2D library has not been downloaded into the destination directory.");
    }
  }

}
