The Whiteboard module can be used to draw by dragging the mouse inside the canvas.

The following DIV CSS properties are not necessary to draw, but are required to view saved whiteboard:

1) position
2) overflow
3) left
4) top
5) width
6) height
7) background-color
8) z-index

These attributes will be elimintated by the default Drupal text format. 'Full HTML' allows whiteboard to be saved but should be used with caution.

DEPENDENCIES

- JavaScript enabled browser
- Markup module
- jsDraw2D library

INSTALLATION

- Enable the Whiteboard module.
- Download the jsDraw2D to /libraries/jsdraw2d/jsdraw2d.js. You may optionally use Drush to easily resolve this dependency with `drush whiteboard:download`.
- Disable text editors (like CKEditor) for text formats to be used with Whiteboard
- Drag and Drop the 'Full HTML' text format to the top of the text format list at /admin/config/content/formats. This will ensure marks will be saved to the whiteboard. You can replace the 'Full HTML' text format with one that end users will use. Please make sure the CSS propeties listed at the top are allowed for DIVs.
- Go to /admin/structure/types/manage/whiteboard/fields/node.whiteboard.field_canvas and allow the 'Full HTML' text format for the field. If you created a special text format just for whiteboard, you can checkmark that one.
- Add new content of type Whiteboard.

LIMITATIONS

- In Drupal 8, drawing closed curves and bezier curves throw JavaScript errors - https://www.drupal.org/project/whiteboard/issues/3065930

ROADMAP

- Support to save whiteboard to the Drupal public/private filesystem - https://www.drupal.org/project/whiteboard/issues/3065926
- Create tests

REFERENCES

- http://jsdraw2d.jsfiction.com/
- https://github.com/sameerb/jsDraw2D
- https://www.drupal.org/project/whiteboard

CREDITS

- The Whiteboard module was created by pmagunia - https://www.drupal.org/u/pmagunia
- https://pmagunia.com
