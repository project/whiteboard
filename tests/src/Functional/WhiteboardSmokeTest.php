<?php

namespace Drupal\Tests\whiteboard\Functional;

use Drupal\Tests\BrowserTestBase;
use Drupal\filter\Entity\FilterFormat;
use Drush\TestTraits\DrushTestTrait;

/**
 * Functional tests for Whiteboard module.
 *
 * @group whiteboard
 */
class WhiteboardSmokeTest extends BrowserTestBase {
  use DrushTestTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'filter',
    'node',
    'user',
    'path',
    'text',
    'markup',
    'whiteboard',
  ];

  /**
   * Theme to enable.
   *
   * @var string
   */
  protected $defaultTheme = 'stark';

  /**
   * A user with administration permissions.
   *
   * @var \Drupal\user\UserInterface
   */

  protected $adminUser;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $full_html_format = FilterFormat::create(
          [
            'format' => 'full_html',
            'name' => 'Full HTML',
            'weight' => 1,
            'filters' => [],
          ]
      );
    $full_html_format->save();

    $this->adminUser = $this->drupalCreateUser(
          [
            'administer filters',
            'administer whiteboard',
            'create whiteboard content',
            'administer site configuration',
          ]
      );

    $this->drupalLogin($this->adminUser);

    $this->drush('whiteboard:download');

  }

  /**
   * Make sure Full HTML text format is enabled.
   */
  public function testFilter() {
    $this->drupalGet('admin/config/content/formats/manage/full_html');
    $web_assert = $this->assertSession();
    $web_assert->statusCodeEquals(200);
    $web_assert->pageTextContains("Full HTML");
  }

  /**
   * Make sure the Settings page loads correctly.
   */
  public function testSettingsPage() {
    $this->drupalGet('admin/config/content/whiteboard');
    $web_assert = $this->assertSession();
    $web_assert->statusCodeEquals(200);
    $web_assert->pageTextContains("Maximum Whiteboard size");
    $web_assert->pageTextContains("Enter the maximum number of marks that a whiteboard may save to the database.");
  }

  /**
   * Make sure the Settings page loads correctly.
   */
  public function testNodePage() {
    $this->drupalGet('node/add/whiteboard');
    $web_assert = $this->assertSession();
    $web_assert->statusCodeEquals(200);
  }

}
