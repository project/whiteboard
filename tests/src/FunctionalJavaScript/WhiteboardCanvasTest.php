<?php

namespace Drupal\Tests\whiteboard\FunctionalJavaScript;

use Drupal\FunctionalJavascriptTests\WebDriverTestBase;
use Drupal\filter\Entity\FilterFormat;
use Drush\TestTraits\DrushTestTrait;

/**
 * Functional tests for Whiteboard module.
 *
 * @group whiteboard
 */
class WhiteboardCanvasTest extends WebDriverTestBase {
  use DrushTestTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'filter',
    'node',
    'user',
    'path',
    'text',
    'markup',
    'whiteboard',
  ];

  /**
   * Theme to enable.
   *
   * @var string
   */
  protected $defaultTheme = 'stark';

  /**
   * A user with administration permissions.
   *
   * @var \Drupal\user\UserInterface
   */

  protected $adminUser;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $full_html_format = FilterFormat::create(
          [
            'format' => 'full_html',
            'name' => 'Full HTML',
            'weight' => 1,
            'filters' => [],
          ]
      );
    $full_html_format->save();

    $this->adminUser = $this->drupalCreateUser(
          [
            'administer filters',
            'administer whiteboard',
            'create whiteboard content',
            'administer site configuration',
          ]
      );

    $this->drupalLogin($this->adminUser);

    $this->drush('whiteboard:download');

  }

  /**
   * Make sure the Settings page loads correctly.
   */
  public function testNodePage() {
    $this->drupalGet('node/add/whiteboard');
    $page = $this->getSession()->getPage();
    $web_assert = $this->assertSession();
    $web_assert->pageTextContains("Whiteboard loaded");
    $xpath = $web_assert->buildXPathQuery('//input[@value=:value]', [':value' => 'Clear Board']);
    $input = $page->findAll('xpath', $xpath);
    $web_assert->assert(count($input) === 1, 'Incorrect number of Clear Board inputs found.');
  }

}
